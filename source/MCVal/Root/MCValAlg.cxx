#include <AsgMessaging/MessageCheck.h>
#include <MCVal/MCValAlg.h>
#include <xAODEventInfo/EventInfo.h>
#include <typeinfo>

MCValAlg ::MCValAlg(const std::string &name,
                    ISvcLocator *pSvcLocator)
    : EL::AnaAlgorithm(name, pSvcLocator)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  This is also where you
  // declare all properties for your algorithm.  Note that things like
  // resetting statistics variables or booking histograms should
  // rather go into the initialize() function.

  declareProperty("pdgIdBSM", m_pdgIdBSM = 4900023, "Truth BSM PDGId");
}

StatusCode MCValAlg ::initialize()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.

  // Create output tree
  ANA_CHECK(book(TTree("analysis", "My analysis ntuple")));
  TTree *mytree = tree("analysis");

  // Declare branches
  mytree->Branch("RunNumber", &m_runNumber);
  mytree->Branch("EventNumber", &m_eventNumber);

  m_darkpion_v = new std::vector<float>();
  m_darkpion_beta = new std::vector<float>();
  m_darkpion_gamma = new std::vector<float>();
  m_darkpion_betagamma = new std::vector<float>();
  m_darkpion_ctau = new std::vector<float>();
  m_darkpion_ctaubetagamma = new std::vector<float>();
  m_darkpion_pt = new std::vector<float>();
  m_darkpion_pz = new std::vector<float>();
  m_darkpion_p = new std::vector<float>();
  m_darkpion_eta = new std::vector<float>();
  m_darkpion_phi = new std::vector<float>();
  m_darkpion_m = new std::vector<float>();
  m_darkpion_pdgid = new std::vector<int>();

  m_bsm_pt = new std::vector<float>();
  m_bsm_pz = new std::vector<float>();
  m_bsm_p = new std::vector<float>();
  m_bsm_eta = new std::vector<float>();
  m_bsm_phi = new std::vector<float>();
  m_bsm_m = new std::vector<float>();
  m_bsm_pdgid = new std::vector<int>();

  mytree->Branch("darkpion_v", &m_darkpion_v);
  mytree->Branch("darkpion_beta", &m_darkpion_beta);
  mytree->Branch("darkpion_gamma", &m_darkpion_gamma);
  mytree->Branch("darkpion_betagamma", &m_darkpion_betagamma);
  mytree->Branch("darkpion_ctau", &m_darkpion_ctau);
  mytree->Branch("darkpion_ctaubetagamma", &m_darkpion_ctaubetagamma);

  mytree->Branch("darkpion_pt", &m_darkpion_pt);
  mytree->Branch("darkpion_pz", &m_darkpion_pz);
  mytree->Branch("darkpion_p", &m_darkpion_p);
  mytree->Branch("darkpion_eta", &m_darkpion_eta);
  mytree->Branch("darkpion_phi", &m_darkpion_phi);
  mytree->Branch("darkpion_m", &m_darkpion_m);
  mytree->Branch("darkpion_pdgid", &m_darkpion_pdgid);
  mytree->Branch("darkpion_N", &m_darkpion_N);

  mytree->Branch("bsm_pt", &m_bsm_pt);
  mytree->Branch("bsm_pz", &m_bsm_pz);
  mytree->Branch("bsm_p", &m_bsm_p);
  mytree->Branch("bsm_eta", &m_bsm_eta);
  mytree->Branch("bsm_phi", &m_bsm_phi);
  mytree->Branch("bsm_m", &m_bsm_m);
  mytree->Branch("bsm_pdgid", &m_bsm_pdgid);
  mytree->Branch("bsm_N", &m_bsm_N);

  m_lep_pt = new std::vector<float>();
  m_lep_eta = new std::vector<float>();
  m_lep_phi = new std::vector<float>();
  m_lep_m = new std::vector<float>();
  m_lep_pdgid = new std::vector<int>();

  mytree->Branch("lep_pt", &m_lep_pt);
  mytree->Branch("lep_eta", &m_lep_eta);
  mytree->Branch("lep_phi", &m_lep_phi);
  mytree->Branch("lep_m", &m_lep_m);
  mytree->Branch("lep_pdgid", &m_lep_pdgid);
  mytree->Branch("lep_N", &m_lep_N);

  m_el_pt = new std::vector<float>();
  m_el_eta = new std::vector<float>();
  m_el_phi = new std::vector<float>();
  m_el_m = new std::vector<float>();

  mytree->Branch("el_pt", &m_el_pt);
  mytree->Branch("el_eta", &m_el_eta);
  mytree->Branch("el_phi", &m_el_phi);
  mytree->Branch("el_m", &m_el_m);
  mytree->Branch("el_N", &m_el_N);

  m_mu_pt = new std::vector<float>();
  m_mu_eta = new std::vector<float>();
  m_mu_phi = new std::vector<float>();
  m_mu_m = new std::vector<float>();

  mytree->Branch("mu_pt", &m_mu_pt);
  mytree->Branch("mu_eta", &m_mu_eta);
  mytree->Branch("mu_phi", &m_mu_phi);
  mytree->Branch("mu_m", &m_mu_m);
  mytree->Branch("mu_N", &m_mu_N);

  m_q_pt = new std::vector<float>();
  m_q_eta = new std::vector<float>();
  m_q_phi = new std::vector<float>();
  m_q_m = new std::vector<float>();
  m_q_pdgid = new std::vector<int>();

  mytree->Branch("q_pt", &m_q_pt);
  mytree->Branch("q_eta", &m_q_eta);
  mytree->Branch("q_phi", &m_q_phi);
  mytree->Branch("q_m", &m_q_m);
  mytree->Branch("q_pdgid", &m_q_pdgid);
  mytree->Branch("q_N", &m_q_N);

  m_jet_pt = new std::vector<float>();
  m_jet_eta = new std::vector<float>();
  m_jet_phi = new std::vector<float>();
  m_jet_m = new std::vector<float>();

  mytree->Branch("jet_pt", &m_jet_pt);
  mytree->Branch("jet_eta", &m_jet_eta);
  mytree->Branch("jet_phi", &m_jet_phi);
  mytree->Branch("jet_m", &m_jet_m);
  mytree->Branch("jet_N", &m_jet_N);

  mytree->Branch("met", &m_met);
  mytree->Branch("met_phi", &m_met_phi);

  // Book output histograms
  ANA_CHECK(book(TH1F("h_N_darkpion", "h_N_darkpion", 100, 0, 2000)));
  ANA_CHECK(book(TH2F("h_m_darkpion_ctaubetagamma_vs_eta", "h_m_darkpion_ctaubetagamma_vs_eta", 200, -5, 5, 200, -10, 800)));
  ANA_CHECK(book(TH2F("h_m_darkpion_ctaubetagamma_vs_pT", "h_m_darkpion_ctaubetagamma_vs_pT", 200, 0, 1000000, 200, -10, 800)));
  ANA_CHECK(book(TH1F("h_ctaubetagamma_bsm", "h_ctaubetagamma_bsm", 200, 0, 4000)));

  ANA_CHECK(book(TH1F("h_N_el", "h_N_el", 5, -0.5, 4.5)));
  ANA_CHECK(book(TH1F("h_pt_el", "h_pt_el", 50, 0, 1000)));
  ANA_CHECK(book(TH1F("h_eta_el", "h_eta_el", 40, -4.0, 4.0)));
  ANA_CHECK(book(TH1F("h_phi_el", "h_phi_el", 32, -3.2, 3.2)));

  ANA_CHECK(book(TH1F("h_N_mu", "h_N_mu", 5, -0.5, 4.5)));
  ANA_CHECK(book(TH1F("h_pt_mu", "h_pt_mu", 50, 0, 1000)));
  ANA_CHECK(book(TH1F("h_eta_mu", "h_eta_mu", 40, -4.0, 4.0)));
  ANA_CHECK(book(TH1F("h_phi_mu", "h_phi_mu", 32, -3.2, 3.2)));

  ANA_CHECK(book(TH2F("h_N_el_vs_N_mu", "h_N_el_vs_N_mu", 5, -0.5, 4.5, 5, -0.5, 4.5)));

  ANA_CHECK(book(TH1F("h_N_lep", "h_N_lep", 5, -0.5, 4.5)));
  ANA_CHECK(book(TH1F("h_pdgid_lep", "h_pdgid_lep", 32, -16.5, 16.5)));
  ANA_CHECK(book(TH1F("h_pt_lep", "h_pt_lep", 50, 0, 1000)));
  ANA_CHECK(book(TH1F("h_eta_lep", "h_eta_lep", 40, -4.0, 4.0)));
  ANA_CHECK(book(TH1F("h_phi_lep", "h_phi_lep", 32, -3.2, 3.2)));

  ANA_CHECK(book(TH1F("h_N_q", "h_N_q", 8, -0.5, 7.5)));
  ANA_CHECK(book(TH1F("h_pt_q", "h_pt_q", 50, 0, 1000)));
  ANA_CHECK(book(TH1F("h_eta_q", "h_eta_q", 40, -4.0, 4.0)));
  ANA_CHECK(book(TH1F("h_phi_q", "h_phi_q", 32, -3.2, 3.2)));
  ANA_CHECK(book(TH1F("h_pdgid_q", "h_pdgid_q", 14, -7.5, 7.5)));

  ANA_CHECK(book(TH1F("h_N_jet", "h_N_jet", 20, -0.5, 19.5)));
  ANA_CHECK(book(TH1F("h_pt_jet", "h_pt_jet", 50, 0, 1000)));
  ANA_CHECK(book(TH1F("h_eta_jet", "h_eta_jet", 40, -4.0, 4.0)));
  ANA_CHECK(book(TH1F("h_phi_jet", "h_phi_jet", 32, -3.2, 3.2)));

  ANA_CHECK(book(TH1F("h_met", "h_met", 50, 0, 1000)));
  ANA_CHECK(book(TH1F("h_met_phi", "h_met_phi", 32, -3.2, 3.2)));

  return StatusCode::SUCCESS;
}

StatusCode MCValAlg ::execute()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  // Truth particles
  const xAOD::TruthParticleContainer *truth_particles = nullptr;
  ANA_CHECK(evtStore()->retrieve(truth_particles, "TruthParticles"));

  std::vector<const xAOD::TruthParticle *> vec_darkpion;
  std::vector<const xAOD::TruthParticle *> vec_lep;
  std::vector<const xAOD::TruthParticle *> vec_nu;
  std::vector<const xAOD::TruthParticle *> vec_el;
  std::vector<const xAOD::TruthParticle *> vec_mu;
  std::vector<const xAOD::TruthParticle *> vec_q;
  double c = 1; // 299792458; // m/s
  double p = 0.0;
  double v = 0.0;
  double beta = 0.0;
  double gamma = 0.0;
  double betagamma = 0.0;
  double ctau = 0.0;
  double ctaubetagamma = 0.0;
  Float_t m = 1500;
  float par[] = {1., 0.02, 10., 10000., 20.}; // {gd, gq, f_pid[GeV], mq[MeV}, m_pid[GeV]} Mz[GeV]

  for (auto tp : *truth_particles)
  {

    // Skip particles that come from hadron decays
    if (!Truth::notFromHadron(tp))
      continue;

    // Skip gluons
    if (MCUtils::PID::isGluon(tp->pdgId()))
      continue;

    // BSM Z' particles
    if (std::abs(tp->pdgId()) == m_pdgIdBSM)
    {
      //	ANA_MSG_INFO ("Got a Zprime!");

      if (tp->status() != 62)
        continue;
      if (tp->parent()->status() == 62)
        printf("UHOH THIS IS DOUBLY COUNTED!!");
      ANA_MSG_INFO(". . . ZPrime about to decay!");
    }


    // BSM dark pions
    // SHould we include off diagonals?
    if (tp->pdgId() == 4900101 || tp->pdgId() == 4900211 || tp->pdgId() == 4900111)
    {
      // if(tp->pdgId() == 4900101 || tp->pdgId() == 4900111 ||tp->pdgId() == 4900211)) {
      //ANA_MSG_INFO("Got a dark pion!");

//      //  Skip if it is a self-decay
//      if (tp->parent()->pdgId() == tp->pdgId())
//        continue;
      //  Skip if it doesnt decay to SM.
//      if (abs(tp->child()->pdgId()) - 4900000 < 1000)
//        continue;
//      std::cout << typeid(tp->child()).name() << '\n'; 
      std::cout << "Self: " << tp->pdgId() << '\n'; 
      std::cout << "Child: " << tp->child() << " , PDGID: " << tp->child()->pdgId() << '\n';
      if (abs(tp->child()->pdgId()) > 490000)
        continue;
      ANA_MSG_INFO(". . . Dark pion about to decay to SM!");
      std::cout << "Child: " << tp->child() << " , PDGID: " << tp->child()->pdgId() << '\n';

      // Store BSM particle
      vec_darkpion.push_back(tp);
    }

    if (Truth::isFinalElectron(tp) && !Truth::isFromPhoton(tp) && Truth::isFromParticle(tp, m_pdgIdBSM))
    {
      vec_el.push_back(tp);
      vec_lep.push_back(tp);
    }

    if (Truth::isFinalMuon(tp) && !Truth::isFromPhoton(tp) && Truth::isFromParticle(tp, m_pdgIdBSM))
    {
      vec_mu.push_back(tp);
      vec_lep.push_back(tp);
    }

    if (Truth::isFinalQuark(tp) && !Truth::isFromGluon(tp) && Truth::isFromParticle(tp, m_pdgIdBSM))
    {
      if (std::abs(tp->pdgId()) != 5)
      {
        vec_q.push_back(tp);
      }
    }
  }

  // Clear darkpion vectors
  m_darkpion_pt->clear();
  m_darkpion_eta->clear();
  m_darkpion_phi->clear();
  m_darkpion_m->clear();
  m_darkpion_pdgid->clear();
  m_darkpion_pz->clear();
  m_darkpion_p->clear();
  m_darkpion_v->clear();
  m_darkpion_beta->clear();
  m_darkpion_gamma->clear();
  m_darkpion_betagamma->clear();
  m_darkpion_ctau->clear();
  m_darkpion_ctaubetagamma->clear();

  // Store number of dark pion particles
  hist("h_N_darkpion")->Fill(vec_darkpion.size());
  m_darkpion_N = vec_darkpion.size();

  // Loop over BSM particles
  for (auto darkpion : vec_darkpion)
  {
    
    // Fill histograms
//    hist("h_m_bsm")->Fill(darkpion->m() / Truth::GeV);
    // Fill vectors for branches
    m_darkpion_pt->push_back(darkpion->pt());
    m_darkpion_pz->push_back(darkpion->pz());
    m_darkpion_eta->push_back(darkpion->eta());
    m_darkpion_m->push_back(darkpion->m());
    m_darkpion_pdgid->push_back(darkpion->pdgId());
    p = pow(pow(darkpion->pz(), 2) + pow(darkpion->pt(), 2), 0.5) / 1000;
    v = pow(p * c, 2) * pow(pow(c, 2) * pow(p, 4) + m * m, -0.5);
    beta = v / c;
    gamma = pow(1 - pow(beta, 2), -0.5);
    betagamma = beta * gamma;
    ctau = 80 * (1 / (pow(par[0], 2) * pow(par[1], 2))) * pow((2 / par[2]), 2) * pow((100 / par[3]), 2) * (2 / par[4]) * pow(m / 1000, 4);
    ctaubetagamma = ctau * betagamma;
    m_darkpion_p->push_back(p);
    printf("Truth part. ID:%5i, status: %i, pT: %f, eta: %f, phi: %f, mass: %f, pz: %f, p[GeV]:%f , ctau: %f \n",
           darkpion->pdgId(), darkpion->status(), darkpion->pt(), darkpion->eta(), darkpion->phi(), darkpion->m(), darkpion->pz(), p, ctau);

    m_darkpion_v->push_back(v);
  
    m_darkpion_beta->push_back(beta);
    m_darkpion_gamma->push_back(gamma);
    m_darkpion_betagamma->push_back(betagamma);
    m_darkpion_ctau->push_back(ctau);
    m_darkpion_ctaubetagamma->push_back(ctaubetagamma);
    hist("h_ctaubetagamma_bsm")->Fill(ctaubetagamma);
    hist("h_m_darkpion_ctaubetagamma_vs_eta")->Fill(darkpion->eta(), ctaubetagamma);
    hist("h_m_darkpion_ctaubetagamma_vs_pT")->Fill(darkpion->pt(), ctaubetagamma);
  }
 
  // Clear electron vectors
  m_el_pt->clear();
  m_el_eta->clear();
  m_el_phi->clear();
  m_el_m->clear();

  // Store number of electrons
  hist("h_N_el")->Fill(vec_el.size());
  m_el_N = vec_el.size();

  // Loop over electrons
  for (auto el : vec_el)
  {
    // Fill histograms
    hist("h_pt_el")->Fill(el->pt() / Truth::GeV);
    hist("h_eta_el")->Fill(el->eta());
    hist("h_phi_el")->Fill(el->phi());
    // Fill vectors for branches
    m_el_pt->push_back(el->pt());
    m_el_eta->push_back(el->eta());
    m_el_phi->push_back(el->phi());
    m_el_m->push_back(el->m());
  }

  // Clear muon vectors
  m_mu_pt->clear();
  m_mu_eta->clear();
  m_mu_phi->clear();
  m_mu_m->clear();

  // Store number of muons
  hist("h_N_mu")->Fill(vec_mu.size());
  m_mu_N = vec_mu.size();

  // Loop over muons
  for (auto mu : vec_mu)
  {
    // Fill histograms
    hist("h_pt_mu")->Fill(mu->pt() / Truth::GeV);
    hist("h_eta_mu")->Fill(mu->eta());
    hist("h_phi_mu")->Fill(mu->phi());
    // Fill vectors for branches
    m_mu_pt->push_back(mu->pt());
    m_mu_eta->push_back(mu->eta());
    m_mu_phi->push_back(mu->phi());
    m_mu_m->push_back(mu->m());
  }

  // Fill 2D lepton multiplicity histogram
  hist("h_N_el_vs_N_mu")->Fill(vec_el.size(), vec_mu.size());

  // Clear lepton vectors
  m_lep_pt->clear();
  m_lep_eta->clear();
  m_lep_phi->clear();
  m_lep_m->clear();
  m_lep_pdgid->clear();

  // Store number of leptons
  hist("h_N_lep")->Fill(vec_lep.size());
  m_lep_N = vec_lep.size();

  // Loop over leptons
  for (auto lep : vec_lep)
  {
    // Fill histograms
    hist("h_pdgid_lep")->Fill(lep->pdgId());
    hist("h_pt_lep")->Fill(lep->pt() / Truth::GeV);
    hist("h_eta_lep")->Fill(lep->eta());
    hist("h_phi_lep")->Fill(lep->phi());
    // Fill vectors for branches
    m_lep_pt->push_back(lep->pt());
    m_lep_eta->push_back(lep->eta());
    m_lep_phi->push_back(lep->phi());
    m_lep_m->push_back(lep->m());
    m_lep_pdgid->push_back(lep->pdgId());
  }

  // Clear quark vectors
  m_q_pt->clear();
  m_q_eta->clear();
  m_q_phi->clear();
  m_q_m->clear();
  m_q_pdgid->clear();

  // Store number of quarks
  hist("h_N_q")->Fill(vec_q.size());
  m_q_N = vec_q.size();

  // Loop over quarks
  for (auto q : vec_q)
  {
    // Fill histograms
    hist("h_pdgid_q")->Fill(q->pdgId());
    hist("h_pt_q")->Fill(q->pt() / Truth::GeV);
    hist("h_eta_q")->Fill(q->eta());
    hist("h_phi_q")->Fill(q->phi());
    // Fill vectors for branches
    m_q_pt->push_back(q->pt());
    m_q_eta->push_back(q->eta());
    m_q_phi->push_back(q->phi());
    m_q_m->push_back(q->m());
    m_q_pdgid->push_back(q->pdgId());
  }

  // Jets
  const xAOD::JetContainer *truth_jets = nullptr;
  ANA_CHECK(evtStore()->retrieve(truth_jets, "AntiKt4TruthDressedWZJets"));

  m_jet_pt->clear();
  m_jet_eta->clear();
  m_jet_phi->clear();
  m_jet_m->clear();

  int nJets = 0;
  for (auto jet : *truth_jets)
  {
    // Skip jets with pt < 20 GeV
    if (jet->pt() / Truth::GeV < 20.)
      continue;

    m_jet_pt->push_back(jet->pt());
    m_jet_eta->push_back(jet->eta());
    m_jet_phi->push_back(jet->phi());
    m_jet_m->push_back(jet->m());

    hist("h_pt_jet")->Fill(jet->pt() / Truth::GeV);
    hist("h_eta_jet")->Fill(jet->eta());
    hist("h_phi_jet")->Fill(jet->phi());

    nJets++;
  }
  hist("h_N_jet")->Fill(nJets);
  m_jet_N = nJets;

  // MET
  const xAOD::MissingETContainer *truth_met = nullptr;
  ANA_CHECK(evtStore()->retrieve(truth_met, "MET_Truth"));

  for (auto met : *truth_met)
  {
    if (met->name() == "NonInt")
    {
      m_met = met->met();
      m_met_phi = met->phi();
    }
  }

  hist("h_met")->Fill(m_met / Truth::GeV);
  hist("h_met_phi")->Fill(m_met_phi);

  tree("analysis")->Fill();

  return StatusCode::SUCCESS;
}

StatusCode MCValAlg ::finalize()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.
  return StatusCode::SUCCESS;
}

MCValAlg ::~MCValAlg()
{

  // Delete the allocated vectors to avoid memory leaks
  delete m_bsm_pt;
  delete m_bsm_eta;
  delete m_bsm_phi;
  delete m_bsm_m;
  delete m_bsm_pdgid;

  delete m_el_pt;
  delete m_el_eta;
  delete m_el_phi;
  delete m_el_m;

  delete m_mu_pt;
  delete m_mu_eta;
  delete m_mu_phi;
  delete m_mu_m;

  delete m_q_pt;
  delete m_q_eta;
  delete m_q_phi;
  delete m_q_m;
  delete m_q_pdgid;

  delete m_jet_pt;
  delete m_jet_eta;
  delete m_jet_phi;
  delete m_jet_m;
}
