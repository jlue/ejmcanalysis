#ifndef MCVal_MCValAlg_H
#define MCVal_MCValAlg_H

#include <AnaAlgorithm/AnaAlgorithm.h>

#include <xAODTruth/TruthParticleContainer.h>
#include <xAODMissingET/MissingETContainer.h>

#include <TH1.h>
#include <TH2.h>
#include <TTree.h>
#include <TLorentzVector.h>

#include "MCVal/TruthUtils.h"

class MCValAlg : public EL::AnaAlgorithm
{
public:
  // this is a standard algorithm constructor
  MCValAlg (const std::string& name, ISvcLocator* pSvcLocator);

  ~MCValAlg () override;

  // these are the functions inherited from Algorithm
  virtual StatusCode initialize () override;
  virtual StatusCode execute () override;
  virtual StatusCode finalize () override;

private:
  // Algorithm properties
  int m_pdgIdBSM;

  // Event properties to save
  unsigned int m_runNumber = 0; ///< Run number
  unsigned long long m_eventNumber = 0; ///< Event number

  float m_met;
  float m_met_phi;

  std::vector<float> *m_darkpion_v;
  std::vector<float> *m_darkpion_beta;
  std::vector<float> *m_darkpion_gamma;
  std::vector<float> *m_darkpion_betagamma;
  std::vector<float> *m_darkpion_ctau;
  std::vector<float> *m_darkpion_ctaubetagamma;

  std::vector<float> *m_darkpion_pz;
  std::vector<float> *m_darkpion_p;
  std::vector<float> *m_darkpion_pt;
  std::vector<float> *m_darkpion_eta;
  std::vector<float> *m_darkpion_phi;
  std::vector<float> *m_darkpion_m;
  std::vector<int> *m_darkpion_pdgid;
  int m_darkpion_N;

  std::vector<float> *m_bsm_v;
  std::vector<float> *m_bsm_beta;
  std::vector<float> *m_bsm_gamma;
  std::vector<float> *m_bsm_betagamma;
  std::vector<float> *m_bsm_ctau;
  std::vector<float> *m_bsm_ctaubetagamma;

  std::vector<float> *m_bsm_pz;
  std::vector<float> *m_bsm_p;
  std::vector<float> *m_bsm_pt;
  std::vector<float> *m_bsm_eta;
  std::vector<float> *m_bsm_phi;
  std::vector<float> *m_bsm_m;
  std::vector<int> *m_bsm_pdgid;
  int m_bsm_N;

  std::vector<float> *m_lep_pt;
  std::vector<float> *m_lep_eta;
  std::vector<float> *m_lep_phi;
  std::vector<float> *m_lep_m;
  std::vector<int> *m_lep_pdgid;
  int m_lep_N;

  std::vector<float> *m_el_pt;
  std::vector<float> *m_el_eta;
  std::vector<float> *m_el_phi;
  std::vector<float> *m_el_m;
  int m_el_N;

  std::vector<float> *m_mu_pt;
  std::vector<float> *m_mu_eta;
  std::vector<float> *m_mu_phi;
  std::vector<float> *m_mu_m;
  int m_mu_N;

  std::vector<float> *m_q_pt;
  std::vector<float> *m_q_eta;
  std::vector<float> *m_q_phi;
  std::vector<float> *m_q_m;
  std::vector<int> *m_q_pdgid;
  int m_q_N;

  std::vector<float> *m_jet_pt;
  std::vector<float> *m_jet_eta;
  std::vector<float> *m_jet_phi;
  std::vector<float> *m_jet_m;
  int m_jet_N;

};

#endif
